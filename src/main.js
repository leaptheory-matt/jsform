import Vue from 'vue';
import App from './App.vue';
import router from './router';
import store from './store';
import VeeValidate from 'vee-validate';
import { Validator } from 'vee-validate';
import { validators } from './helpers/validators.js';

const configElement = document.getElementById( 'LeapForm' );
const CONFIG = JSON.parse( configElement.innerHTML );

Vue.config.productionTip = false;



const validateConfig = {
  aria: true,
  classNames: {},
  classes: true,
  events: 'input|blur|change|focus',
};

Vue.use(VeeValidate, validateConfig);
Validator.extend('validateABA', validators.validateABA);
Validator.extend('validateZip', validators.validateZip);
Validator.extend('validateAreaCode', validators.validateAreaCode);
Validator.extend('validateAge', validators.validateAge);

new Vue({
  router,
  store,
  render: h => h(App),
  data: {
    siteType: CONFIG.type,
    siteURL: CONFIG.site_url,
    webmasterID: CONFIG.webmaster_id,
    webmasterID2: CONFIG.webmaster_id2 || CONFIG.webmaster_id,
    subaccount: CONFIG.subaccount,
    gc: CONFIG.gc || false,
    declineURL: CONFIG.decline_url || "none",
  }
}).$mount('#jsform')
