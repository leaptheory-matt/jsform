import Vue from 'vue';
import Router from 'vue-router';
import Form from './views/Form.vue';
import FormStep1 from '@/views/form/FormStep1.vue';
import FormStep2 from '@/views/form/FormStep2.vue';
import FormStep3 from '@/views/form/FormStep3.vue';
import FormStep4 from '@/views/form/FormStep4.vue';
import FormStep5 from '@/views/form/FormStep5.vue';
import Signin from '@/views/form/Signin.vue';
import Login from '@/views/form/Login.vue';
import Interstitial from '@/views/form/Interstitial.vue';

import FormRecognise from '@/views/form/FormRecognise.vue';
import Returning from '@/views/form/Returning.vue';
import store from './store';
import VueScrollTo from 'vue-scrollto';

Vue.use(Router);

//export default new Router({
const router = new Router({
  mode: 'hash',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'registration',
      children: [
      {
        name: 'welcome',
        path: 'welcome',
        component: FormRecognise,
      },
      {
        name: 'signin',
        path: 'signin',
        component: Signin,
      },
      {
        name: 'login',
        path: 'login',
        component: Login,
      },
      {
        name: 'step1',
      	path: '1',
        component: FormStep1,
        alias: ['/']
      },
      {
        name: 'step2',
      	path: '2',
        beforeEnter: (to, from, next) => {
            if (!store.state.home.formData.firstStep) {
                next('/1');
            } else {
                next();
            }
        },
        component: FormStep2
      }, 
      {
        name: 'step3',
        path: '3',
        beforeEnter: (to, from, next) => {
            if (!store.state.home.formData.secondStep) {
                next('/2');
            } else {
                next();
            }
        },  
        component: FormStep3
      }, 
      {
        name: 'step4',
        path: '4',
        beforeEnter: (to, from, next) => {
            if (!store.state.home.formData.thirdStep) {
                next('/3');
            } else {
                next();
            }
        },
        component: FormStep4
      }, 
      {
        name: 'step5',
        path: '5',
        beforeEnter: (to, from, next) => {
            if (!store.state.home.formData.fourthStep) {
                next('/4');
            } else {
                next();
            }
        },
        component: FormStep5
      },
      {
        name: 'declined',
        path: 'declined',
        beforeEnter: (to, from, next) => {
            if (store.state.home.formData.apiResponse !== 'interstitial') {
                next('/');
            } else {
                next();
            }
        },
        component: Interstitial
      }, 
      {
        name: 'returning',
        path: 'returning',
        beforeEnter: (to, from, next) => {
            if (!store.state.home.auth.confirmedUser) {
                next('/');
            } else {
                next();
            }
        },
        component: Returning
      },

    ],
      component: Form
    },
  ],
  scrollBehavior (to, from, savedPosition) {

    if (to.hash) {
        VueScrollTo.scrollTo(to.hash, 700);
        return { selector: to.hash }
    } else {
        return { x: 0, y: 0 }
    }
    //return { x: 0, y: 0 }
  }  
});

export default router;