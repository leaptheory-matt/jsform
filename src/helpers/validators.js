import axios from 'axios';
import moment from 'moment';

export const validators = {
    'validateAge': {
        getMessage: field => 'You must be over 18 to apply.',
        validate: value => {
            
            //age18
            let checkDate = moment().subtract(18, 'years');
            //age100
            let checkDate2 = moment().subtract(100, 'years');
            let bd = moment(value, 'MM/DD/YYYY');
            return bd < checkDate && bd > checkDate2;
        }
    },
    'validateABA': {
        getMessage: field => 'This value seems to be invalid.',
        validate: value => {
            let c = 0;
            let isValid = false;

            if (value && value.length == 9) {//don’t waste energy totalling if its not 9 digits
                for (let i = 0; i < value.length; i += 3) {//total the sums
                    c += parseInt(value.charAt(i), 10) * 3 + parseInt(value.charAt(i + 1), 10) * 7 + parseInt(value.charAt(i + 2), 10);
                }
                isValid = c != 0 && c % 10 == 0;//check if multiple of 10
            }
            return isValid;
        }
    },
    'validateZip': {
        getMessage: field => 'Sorry, we do not currently process loans in your state.',
        validate: async value => {
            if(value.length != 5){
                return false;
            }
            try {
                const response = await axios.get(`${process.env.VUE_APP_LOCAL_API}zipcode/${value}`);
                if(response.data.found) {
                    return true;
                } else {
                    return false;
                }
            } catch(err) {
                console.log(err);
                return false;
            }
            axios.get()
            return false;
        },
    },
    'validateAreaCode': {
        getMessage: field => 'Oops! It looks like that number is not real.',
        validate: async value => {
            if(value.length != 12 && value.length != 14){
                return false;
            }
            if(value.substr(0,3) == '1-8' || value.substr(0,1) == '8'){
                return true;
            } else {
                try {
                    const response = await axios.get(`${process.env.VUE_APP_LOCAL_API}areacode/${value}`);
                    if(response.data.found) {
                        return true;
                    } else {
                        return false;
                    }
                } catch(err) {
                    console.log(err);
                    return false;
                }
            }
        },
    }
}