import axios from 'axios';

const state = {
    visitData: {
        clientIP: '',
        refererURL: '',
        userAgent: '',
        subid1: '',
        subid2: '',
        subid3: '',
        subid4: '',
        clickID: '',
        sessionID: '',
        webmasterID: '',
        webmasterID2: '',
        subaccount: '',
    },
    auth: {
      useragentRecognised: false,
      recognisedEmail: false,
      recognisedUser: false,
      confirmedUser: false,
      userDetails: {}
    },
    formData: {},
    loanAmounts: [
      { type: 'IL',
        amounts: [
          {value: 500, min: 100, max: 500, range: '100;500', label: "$100 - $500"},
          {value: 800, min: 501, max: 1000, range: '500;1000', label: "$500 - $1000"},
          {value: 1000, min: 1001, max: 1500, range: '1000;1500', label: "$1000 - $1500"},
          {value: 2000, min: 1501, max: 2000, range: '1500;2000', label: "$1500 - $2000"},
          {value: 3000, min: 2001, max: 3000, range: '2000;3000', label: "$2000 - $3000"},
          {value: 4000, min: 3001, max: 4000, range: '3000;4000', label: "$3000 - $4000"},
          {value: 5000, min: 4001, max: 5000, range: '4000;5000', label: "$4000 - $5000"}
        ]
      },
      { type: 'PDL',
        amounts: [
          {value: 500, min: 100, max: 500, range: '100;500', label: "$100 - $500"},
          {value: 800, min: 501, max: 1000, range: '500;1000', label: "$500 - $1000"},
          {value: 1000, min: 1001, max: 1500, range: '1000;1500', label: "$1000 - $1500"},
          {value: 2000, min: 1501, max: 2000, range: '1500;2000', label: "$1500 - $2000"}
        ]
      },
      { type: 'PDLs',
      amounts: [
        {value: 250, min: 100, max: 250, range: '100;250', label: "$100 - $250"},
        {value: 500, min: 251, max: 500, range: '250;500', label: "$250 - $500"},
        {value: 750, min: 501, max: 750, range: '500;750', label: "$500 - $750"},
        {value: 1000, min: 751, max: 1000, range: '750;1000', label: "$750 - $1000"}
      ]
    }
    ]
  };
  
  const getters = {
    allSettings: state => state.settings,
    loanAmounts: state => state.loanAmounts,
    formData: state => state.formData,
    auth: state => state.auth,
    visitData: state => state.visitData,
  };
  
  const actions = {
    toggleLeadForm({commit}) {
      commit('toggleLeadForm');
    },
    closeLoginForm({commit}) {
        commit('toggleUseragentRecogniseUser', {recognised: false, name: ''});
        commit('toggleRecogniseEmail', {recognised: false, name: ''})
    },
    saveFormData({commit}, data) {
      commit('saveFormData', data);
    },
    recogniseUser({commit}, set) {
      commit('toggleRecogniseUser', set);
    },
    authoriseUser({commit}, data) {
      commit('toggleAuthoriseUser', data);
    },
    saveVisitData({commit}, data) {
        let clickFunction = 1;
        let sessionID = '';

        //Check if session data exists locally and is still valid
        if(localStorage.getItem('session')){
            const session = JSON.parse(localStorage.getItem('session'));
            if(session.sessionID !== ''){
                const now = new Date(); 
                if(now < session.expires) {
                    //Session is valid, check for matches
                    console.log('Session found');
                    if(
                        session.webmasterID == data.webmasterID && 
                        session.webmasterID2 == data.webmasterID2 &&
                        session.subaccount == data.subaccount && 
                        session.subid1 == data.subid1 &&
                        session.subid2 == data.subid2 &&
                        session.subid3 == data.subid3 &&
                        session.subid4 == data.subid4
                    ) {
                        console.log('Session match.');
                        clickFunction = 2;
                        sessionID = session.sessionID;
                    } else {
                        console.log("Session data mismatch. Load new session.");
                        localStorage.removeItem('session');
                    }
                } else {
                    //Session has expired, remove it
                    console.log('Session has expired');
                    localStorage.removeItem('session');
                }
            } else {
                console.log('No session found');
            }
        }

        const params = {
            clickFunction: clickFunction,
            ...data,
            sessionID: sessionID
        };

        axios.post(`${process.env.VUE_APP_LOCAL_API}click/`, params).then(response => {
            if(response.data.clickID){
                commit('saveClickID', response.data.clickID);
            }
            if(response.data.sessionID){
                commit('saveSessionID', {...params, sessionID: response.data.sessionID});
            }
        }).catch( err => {
            console.log(err);
        });

        data.sessionID = sessionID;
        commit('saveVisitData', data);
    },
    registerPoint({state}, data) {
        const params = {
            sessionID: state.visitData.sessionID,
            point: data
        };

        axios.post(`${process.env.VUE_APP_LOCAL_API}point/`, params).then(response => {
            if(response.data.success){
                //Success handler
            } else {
                //Failure handler
            }
        }).catch( err => {
            console.log(err);
        });
    },
    checkEmail({commit}, data) {
        const endpoint = `${process.env.VUE_APP_LOCAL_API}recognizeEmail?email=${data.email.toLowerCase()}`;
        axios.get(endpoint).then(response => {
            if(response.data.success){
                commit('toggleRecogniseEmail', {recognised: true, name: data.name});
            }
        }).catch(err => {
            console.log(err);
        });
    },
    checkUserAgentHistory({commit, state}) {
        const endpoint = `${process.env.VUE_APP_LOCAL_API}recognizeAgent?ip=${state.visitData.clientIP}&useragent=${state.visitData.userAgent}`;
        axios.get(endpoint).then(response => {
            if(response.data.success){
                commit('toggleUseragentRecogniseUser', {recognised: true, name: response.data.name});
            }
        }).catch( err => {
            console.log(err);
        });
    },
    async verifyAgent({commit, state}, data) {
        const endpoint = `${process.env.VUE_APP_LOCAL_API}verifyAgent?ip=${state.visitData.clientIP}&useragent=${state.visitData.userAgent}&zipcode=${data.zipcode}&ssn_last4=${data.ssnLast4}`;
		try {
            const response = await axios.get(endpoint);
			if(response.data.body){
                commit('toggleAuthoriseUser', {
                    clientKey: response.data.clientKey,
                    ...response.data.body
                });
                return true;
			} else {
                return false;
            }
		} catch(err) {
            console.log(err);
            return false;
		}
    },
    async sendToAPI({commit, state}) {

        const params = {
            "verticalID": 'payday',
            "webmasterID": state.formData.apiResponse == 'interstitial' ? state.visitData.webmasterID2 : state.visitData.webmasterID,
            "clientIP": state.visitData.clientIP,
            "userAgent": state.visitData.userAgent,
            "refererURL": state.visitData.refererURL,
            "declineURL": state.visitData.declineURL,
            "subaccount": state.visitData.subaccount,
            "sessionID": state.visitData.sessionID,
            "subid1": state.visitData.subid1,
            "subid2": state.visitData.subid2,
            "subid3": state.visitData.subid3,
            "subid4": state.visitData.subid4,

            "loanAmount": state.formData.loan_amount_raw,
            "loanAmountRange": state.formData.loan_amount_range,
            "loanPurpose": state.formData.loanPurpose,
            "creditScore": state.formData.creditScore,
            "firstName": state.formData.firstName,
            "lastName": state.formData.lastName,
            "email": state.formData.email,
            "dob": reformatDate(state.formData.dob),
            "zip": state.formData.zipcode,
            "city": state.formData.city,
            "state": state.formData.address_state,
            "address": state.formData.address,
            "monthsAtAddress": state.formData.monthsAtAddress,
            "homeOwnership": state.formData.homeOwnership,
            "driversLicense": state.formData.driversLicense,
            "driversLicenseState": state.formData.driversLicenseState,
            "cellPhone": state.formData.phone,
            "homePhone": state.formData.phone,
            "ssn": state.formData.ssn,
            "employmentType": state.formData.employmentType,
            "employerName": state.formData.employerName,
            "monthsEmployed": state.formData.monthsEmployed,
            "monthlyIncome": state.formData.monthlyIncome,
            "payFrequency": state.formData.payFrequency,
            "nextPayDate": state.formData.nextPaydate,
            "secondPayDate": state.formData.secondPayDate,
            "workPhone": state.formData.workPhone,
            "workExt": state.formData.workExt,
            "payType": state.formData.payType,
            "bankRoutingNumber": state.formData.bankRoutingNumber,
            "bankName": state.formData.bankName,
            "bankAccountNumber": state.formData.bankAccountNumber,
            "bankAccountType": state.formData.bankAccountType,
            "monthsAtBank": state.formData.monthsAtBank,
            "click_clickID": state.visitData.clickID,
        };

        if(state.formData.clientKey){
            params.clientKey = state.formData.clientKey;
        }

        try {
            const response = await axios.post(`${process.env.VUE_APP_LOCAL_API}leads/`, params);
            commit('saveFormData', { apiResponse: response.data.status, apiRedirectURL: response.data.redirectURL} );

            //save login
            localStorage.setItem('user', JSON.stringify({email: state.formData.email, name: state.formData.firstName}));
            if(response.data.redirectURL) {
                return response.data.redirectURL;
            } else {
                return false;
            }
        } catch(err) {
            console.log('Error Sending:', err);
            return false;
        }
    }
  };
  
  const mutations = {
    toggleLeadForm: (state) => state.settings.showLeadForm = !state.settings.showLeadForm,
    saveFormData: (state, data) => {
      state.formData = {
        ...state.formData, 
        ...data
      }
    },
    toggleUseragentRecogniseUser: (state, data) => {
        state.auth.useragentRecognised = data.recognised;
        state.auth.userDetails.name = data.name;
    },
    toggleRecogniseUser: (state, set) => {
      state.auth.recognisedUser = set
    },
    toggleRecogniseEmail: (state, data) => {
        state.auth.userDetails.name = data.name;
        state.auth.recognisedEmail = data.recognised;
    },
    toggleAuthoriseUser: (state, data) => {
      state.auth.confirmedUser = true;
      state.auth.userDetails = data;
    },
    saveVisitData: (state, data) => {
        state.visitData = data;
    },
    saveClickID: (state, clickID) => {
        state.visitData.clickID = clickID
    },
    saveSessionID: (state, data) => {
        state.visitData.sessionID = data.sessionID;
        //save to local storage
        const now = new Date();
        localStorage.setItem('session', 
            JSON.stringify({ 
                subid1: data.subid1,
                subid2: data.subid2,
                subid3: data.subid3,
                subid4: data.subid4,
                sessionID: data.sessionID,
                webmasterID: data.webmasterID,
                webmasterID2: data.webmasterID2,
                subaccount: data.subaccount,
                expires: now.getTime() + (24*60*60*1000)
            })
        );
    }
  };
  
  export default {
    state,
    getters,
    actions,
    mutations
  };



function reformatDate(dateString = '01/01/1970'){
    const formatted = dateString.match(/^(\d{2})\/(\d{2})\/(\d{4})$/);
    if (formatted) {
        return formatted[3] + '-' + formatted[1] + '-' + formatted[2];
    } else {
        return '1970-01-01';
    }
}